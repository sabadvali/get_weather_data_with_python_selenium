from datetime import datetime
import threading
from flask import Flask,jsonify
from urls import *
from flask_cors import CORS
import requests
import os
from apscheduler.schedulers.background import BackgroundScheduler



log_filename = "Response_log"
script_path = os.path.dirname(os.path.realpath(__file__))

"""
ფუნქცია, რომელიც გადაცემულ message - ს ჩაწერს  მითტითებულ log ფაილში
ან დაბეჭდავს ტერმინალში
""" 
def print_and_log(message, empty_line=False):
    log_file_path = script_path + "/" + log_filename
    pid  = os.getpid()
    with open(log_file_path, "a") as log_file:
        if not empty_line:
            log_file.write("[" + datetime.now().strftime('%Y-%m-%d %T') + "] [" + str(pid) + "] " + message + "\n")
        else:
            log_file.write("\n")


app = Flask(__name__,template_folder='./templates')
CORS(app)

scheduler = BackgroundScheduler(daemon=True)
scheduler.start()


def Get_data_from_WW():
    for i in urls_for_Get_data_from_WW:
        try:
            response = requests.get(i["api"])   

            if response.status_code == 200:
                data = response.json()

                try:
                    PRECIP_RATE = data['observations'][0]['metric']['precipRate']
                    PRECIP_ACCUM = data['observations'][0]['metric']['precipTotal']
                    PRECIP_RATE = "{:.2f}".format(PRECIP_RATE)
                    PRECIP_ACCUM = "{:.2f}".format(PRECIP_ACCUM)
                except:
                    print("json დან მონაცემების ამოღების დროს მოხდა შეცდომა")

                i["PRECIP_RATE"] = PRECIP_RATE
                i["PRECIP_ACCUM"] = PRECIP_ACCUM

                PRECIP_ACCUM = float(PRECIP_ACCUM)

                if PRECIP_ACCUM == 0.0:
                    i["top_bottom"] = i["static_px"]
                    i["first_div_height"] = 0.00
                else:
                    i["top_bottom"] = i["static_px"] - PRECIP_ACCUM
                    i["first_div_height"] = PRECIP_ACCUM
               # print(data['observations'][0]['metric']['precipRate'])
               # print(data['observations'][0]['metric']['precipTotal'])
            else:
                print("ვერ მოხდა მონაცემის წამოღება სტატუს კოდი: ", response.status_code)
                i["first_div_height"] = 0.00
                i["PRECIP_RATE"] = "--,--"
                i["PRECIO_ACCUM"] = "--,--"
        except:
            print("შეცდომა მონაცემების წამოღების დროს Api საშვალებით.")

scheduler.add_job(Get_data_from_WW, 'interval', seconds=30)



@app.route('/get_data', methods=['GET'])
def get_data(): 
        
    Data_for_html= {
                      "urls_for_Get_data_from_WW": urls_for_Get_data_from_WW,
                   }

    return jsonify(Data_for_html)



if __name__ == "__main__":

    ssl_context = ('./apache-certificate.crt', './apache.key')
		
    app.run(host="მიუთითეთ თქვენი აიპი მისამართი", port="პორტი",  ssl_context=ssl_context)

