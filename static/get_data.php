

<?php
$url = 'https:თქვენი_აიპი_მისამართი:პორტი/get_data'; 

$options = [
    'ssl' => [
        'verify_peer' => false,
        'verify_peer_name' => false
    ]
];

$context = stream_context_create($options);
$jsonData = file_get_contents($url, false, $context);

if ($jsonData !== false) {
    $decodedData = json_decode($jsonData, true); 

    if ($decodedData !== null) {

        $responseData = json_encode($decodedData);

        header('Content-Type: application/json');

        echo $responseData;
    } else {
        echo json_encode(['error' => 'Unable to decode JSON']);
    }
} else {
    echo json_encode(['error' => 'Unable to fetch data from URL']);
}
?>
